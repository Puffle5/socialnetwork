package start.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import static start.constants.Constants.HOME_PAGE;

@Controller
public class HomeController {

    @GetMapping("/")
    public String showHome() {
        return HOME_PAGE;
    }
}
