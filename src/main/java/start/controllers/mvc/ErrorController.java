package start.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static start.constants.Constants.ERROR_PAGE;

@Controller
@RequestMapping
public class ErrorController {

    @GetMapping
    public String errorShowPage() {
        return ERROR_PAGE;
    }
}
