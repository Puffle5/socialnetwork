package start.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import start.entities.User;
import start.exceptions.EntityNotFoundException;
import start.services.contracts.RelationshipService;
import start.services.contracts.UserService;

import java.security.Principal;

import static start.constants.Constants.*;

@Controller
@RequestMapping("/profile")
public class ProfileController {

    private static final String ACCOUNT_SETTINGS_PAGE = "account-settings";
    private static final String NEW_FEEDS_PAGE = "new-feeds";
    private static final String OTHER_USER_PAGE = "other-user-page";
    private static final String REDIRECT_PROFILE_USER_PAGE = "redirect:/profile/user/";
    private UserService userService;
    private RelationshipService relationshipService;

    @Autowired
    public ProfileController(UserService userService, RelationshipService relationshipService) {
        this.userService = userService;
        this.relationshipService = relationshipService;
    }

    @GetMapping
    public String profile(Principal principal, Model model) {
        try {
            User user = userService.getUserByName(principal.getName());
            model.addAttribute("user", user);

            return PROFILE_PAGE;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/settings")
    public String profileAccountSettings(Principal principal, Model model) {
        User user = userService.getUserByName(principal.getName());
        model.addAttribute("user", user);

        return ACCOUNT_SETTINGS_PAGE;
    }

    @GetMapping("/new-feeds")
    public String showNewFeeds() {
        return NEW_FEEDS_PAGE;
    }

    @GetMapping("/user/{username}")
    public String showPersonProfilePage(@PathVariable String username, Model model) {
        try {
            User user = userService.getUserByName(username);
            model.addAttribute("user", user);

            return OTHER_USER_PAGE;
        } catch (EntityNotFoundException e) {
            return ERROR_PAGE;
        }
    }

    @GetMapping("/user/decline/{username}")
    public String declinePersonFriendRequest(@PathVariable String username) {
        relationshipService.declineFriendRequest(username);

        return REDIRECT_PROFILE_USER_PAGE + username;
    }

    @GetMapping("/user/delete/{username}")
    public String deleteUserByName(@PathVariable String username) {
        userService.deleteUser(username);

        return REDIRECT_ADMIN_PAGE;
    }
}
