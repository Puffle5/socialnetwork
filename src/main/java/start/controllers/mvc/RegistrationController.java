package start.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import start.email.JavaMailUtil;
import start.entities.User;
import start.replace.RemoveWhiteSpaces;
import start.services.contracts.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static start.constants.Constants.HOME_PAGE;
import static start.constants.Constants.LOGIN_PAGE;

@Controller
public class RegistrationController {

    private static final String CREDENTIALS_VALIDATION_MESSAGE = "Username/password can't be less than 3 symbols/email not correct format.";
    private static final String REGISTER_PAGE = "register";
    private static final String USERNAME_ALREADY_EXIST_MESSAGE = "User with same username already exists!";
    private static final String DEFAULT_COVER_PICTURE = "default_cover.jpg";
    private static final String DEFAULT_PROFILE_PICTURE = "default_profile.jpg";
    private UserDetailsManager userDetailsManager;
    private UserService userService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, UserService userService, PasswordEncoder passwordEncoder) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model, Principal principal) {
        if (principal != null) {
            return HOME_PAGE;
        } else {
            model.addAttribute("user", new User());
            return REGISTER_PAGE;
        }
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) throws Exception {
        String username = RemoveWhiteSpaces.removeWhiteSpaces(user.getUsername());

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", CREDENTIALS_VALIDATION_MESSAGE);
            return REGISTER_PAGE;
        }

        if (userDetailsManager.userExists(username) || userDetailsManager.userExists(username + "deleteUser")) {
            model.addAttribute("error", USERNAME_ALREADY_EXIST_MESSAGE);
            return REGISTER_PAGE;
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        username,
                        passwordEncoder.encode(user.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);
        User currentUser = userService.getUnderTheHoodUser(username);
        currentUser.setEmail(user.getEmail());
        currentUser.setCoverPhotoURL(DEFAULT_COVER_PICTURE);
        currentUser.setPhotoURL(DEFAULT_PROFILE_PICTURE);
        currentUser.setActive(true);
        userService.save(currentUser);
        JavaMailUtil.sendMail(currentUser);

        return LOGIN_PAGE;
    }

    @GetMapping("/register-confirmation")
    public String registerConfirmation() {
        return LOGIN_PAGE;
    }
}