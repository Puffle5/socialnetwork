package start.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import start.entities.User;
import start.services.contracts.UserService;

import java.security.Principal;

@Controller
@RequestMapping()
public class AdminController {

    private static final String ADMIN_PANEL_PAGE = "admin-panel";
    private UserService userService;

    @Autowired
    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/admin")
    public String adminPanel(Principal principal, Model model) {
        User user = userService.getUserByName(principal.getName());
        model.addAttribute("user", user);

        return ADMIN_PANEL_PAGE;
    }
}
