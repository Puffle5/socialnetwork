package start.constants;

public class Constants {

    public static final String REDIRECT_ADMIN_PAGE = "redirect:/admin";
    public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/resources/static/images";
    public static final String ERROR_PAGE = "error";
    public static final String HOME_PAGE = "home";
    public static final String LOGIN_PAGE = "login";
    public static final String NOT_AUTHORIZED_MESSAGE = "You are not authorized.";
    public static final String PROFILE_PAGE = "profile";
}
