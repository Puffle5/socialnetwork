package start.services.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import start.entities.User;
import start.entities.models.UserBindingModel;
import start.exceptions.EntityNotFoundException;
import start.exceptions.NotAuthorizedException;
import start.repositories.UserRepository;
import start.services.contracts.UserService;

import java.util.ArrayList;
import java.util.List;

import static start.constants.Constants.NOT_AUTHORIZED_MESSAGE;

@Service
public class UserServiceImpl implements UserService {

    private static final String NO_EXISTING_USERS_MESSAGE = "There are no users.";
    private static final String NO_EXISTING_USER_NAMES_MESSAGE = "No existing names.";
    private static final String INVALID_USERNAME_MESSAGE = "There is no user with that name.";
    private static final String NO_EXISTING_USER_WITH_THAT_ID_MESSAGE = "There is no user with that id.";
    private static final String NO_EXISTING_USER_MAILS_MESSAGE = "No existing emails.";
    private static final String DELETE_USER_LABEL = "deleteUser";
    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers(String username) {
        User loginUser = userRepository.getByUsernameAndActiveTrue(username);
        if (loginUser == null) {
            throw new NotAuthorizedException(NOT_AUTHORIZED_MESSAGE);
        }

        List<User> users = userRepository.getAllByActiveTrue();
        if (CollectionUtils.isEmpty(users)) {
            throw new EntityNotFoundException(NO_EXISTING_USERS_MESSAGE);
        }

        return users;
    }

    @Override
    public List<String> getAllUsersName() {
        List<String> userNames = new ArrayList<>();
        List<User> users = userRepository.getAllByActiveTrue();
        users.forEach(user -> userNames.add(user.getUsername()));
        if (CollectionUtils.isEmpty(userNames)) {
            throw new EntityNotFoundException(NO_EXISTING_USER_NAMES_MESSAGE);
        }

        return userNames;
    }

    @Override
    public User getUserByName(String name) {
        User user = userRepository.getByUsernameAndActiveTrue(name);
        if (user == null) {
            throw new EntityNotFoundException(INVALID_USERNAME_MESSAGE);
        }

        return user;
    }

    @Override
    public User getUnderTheHoodUser(String name) {
        User user = userRepository.getUserByUsername(name);
        if (user == null) {
            throw new EntityNotFoundException(INVALID_USERNAME_MESSAGE);
        }

        return user;
    }

    @Override
    public User getUserById(Integer id) {
        User user = userRepository.getByIDAndActiveTrue(id);
        if (user == null) {
            throw new EntityNotFoundException(NO_EXISTING_USER_WITH_THAT_ID_MESSAGE);
        }

        return user;
    }

    @Override
    public List<String> getAllUserEmails() {
        List<String> userEmail = new ArrayList<>();
        for (User user : userRepository.getAllByActiveTrue()) {
            userEmail.add(user.getEmail());
        }

        if (userEmail.isEmpty()) {
            throw new EntityNotFoundException(NO_EXISTING_USER_MAILS_MESSAGE);
        }

        return userEmail;
    }

    @Override
    public User updateInfoUser(UserBindingModel userBindingModel, User user) {
        user.setEmail(userBindingModel.getEmail());
        userRepository.saveAndFlush(user);

        return user;
    }

    @Override
    public void deleteUser(String username) {
        User userToDelete = userRepository.getByUsernameAndActiveTrue(username);
        if (userToDelete == null) {
            throw new EntityNotFoundException(INVALID_USERNAME_MESSAGE);
        }

        userToDelete.setActive(false);
        userToDelete.setUsername(userToDelete.getUsername() + DELETE_USER_LABEL);
        save(userToDelete);
    }

    @Override
    public void save(User userToSave) {
        userRepository.saveAndFlush(userToSave);
    }

    @Override
    public User loginUser(String username) {
        User loginUser = userRepository.getByUsernameAndActiveTrue(username);
        if (loginUser == null) {
            throw new NotAuthorizedException(NOT_AUTHORIZED_MESSAGE);
        }

        return loginUser;
    }
}
