package start.configSecurity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String SPRING_DATASOURCE_URL = "spring.datasource.url";
    private static final String SPRING_DATASOURCE_USERNAME = "spring.datasource.username";
    private static final String SPRING_DATASOURCE_PASSWORD = "spring.datasource.password";
    private static final String USER_ROLE = "USER";
    private static final String ADMIN_ROLE = "ADMIN";
    private static final String PROFILE_MATCHER_URL = "/profile/**";
    private static final String ADMIN_PAGE_URL = "/admin";
    private static final String LOGIN_PAGE_URL = "/login";
    private static final String AUTHENTICATE_PAGE_URL = "/authenticate";
    private static final String ACCESS_DENIED_PAGE_URL = "/access-denied";
    private String dbUrl, dbUsername, dbPassword;

    public SecurityConfiguration(Environment environment) {
        dbUrl = environment.getProperty(SPRING_DATASOURCE_URL);
        dbUsername = environment.getProperty(SPRING_DATASOURCE_USERNAME);
        dbPassword = environment.getProperty(SPRING_DATASOURCE_PASSWORD);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(securityDataSource());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(PROFILE_MATCHER_URL)
                .hasAnyRole(USER_ROLE, ADMIN_ROLE)
                .antMatchers(ADMIN_PAGE_URL)
                .hasRole(ADMIN_ROLE)
                .and()
                .formLogin()
                .loginPage(LOGIN_PAGE_URL)
                .loginProcessingUrl(AUTHENTICATE_PAGE_URL)
                .permitAll()
                .and()
                .csrf()
                .disable()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling()
                .accessDeniedPage(ACCESS_DENIED_PAGE_URL);
    }

    @Bean
    public DataSource securityDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);

        return dataSource;
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(securityDataSource());

        return jdbcUserDetailsManager;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
