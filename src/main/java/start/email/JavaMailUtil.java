package start.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import start.entities.User;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class JavaMailUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(JavaMailUtil.class);

    /**
     * Settings for the email.
     *
     * @param recipient User who will receive the mail.
     * @throws Exception
     */
    public static void sendMail(User recipient) throws Exception {
        LOGGER.info("Preparing to send email.");
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        String myAccountEmail = "pms.team.sf@gmail.com";
        String password = "pms1234$";
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(myAccountEmail, password);
            }
        });
        Message message = prepareMessage(session, myAccountEmail, recipient);
        Transport.send(message);
        LOGGER.info("Message sent successfully!");
    }

    /**
     * Preparing message for the email
     *
     * @param session        from where we take data.
     * @param myAccountEmail mail  address from where we send the email
     * @param recipient      mail address for recipient
     * @return populated message
     */
    private static Message prepareMessage(Session session, String myAccountEmail, User recipient) {
        try {
            StringBuilder emailContent = new StringBuilder();
            emailContent.append("<h1>Welcome to Illumni!</h1>").append(
                    "<h4>Hello, ").append(recipient.getUsername()).append(" ").append(", </h4>").append(
                    "<br>").append(
                    "<p>You have been successfully register to <strong>Illumni.</strong></p>").append(
                    "<br>").append(
                    "<p>Your username is:</p>").append(
                    recipient.getUsername()).append(
                    "<br> <br>")
                    .append("<p>")
                    .append("<br>")
                    .append("<br>Best Regards,<br/>")
                    .append("<em>Illumni Team!</em>")
                    .append("</p>")
                    .append("<img src=\"https://i.ibb.co/7YXhj1m/logo-invert.png\" width=\"350\" height=\"200\"> ");
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(myAccountEmail));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient.getEmail()));
            message.setSubject("Registration in Illumni was successful.");
            message.setContent(
                    emailContent.toString(), "text/html");
            return message;
        } catch (Exception e) {
            LOGGER.error("Sending e-mail failed!", e);
        }
        return null;
    }
}
